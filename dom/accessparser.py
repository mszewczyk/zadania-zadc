# -*- coding: utf-8 -*-


import couchdb
from MapReduce import MapReduce
import time


LastIpAddressInLog = ""
New = 0
Updated = 0
NotTouched = 0
Wait=180


def mapToCountingTheNumberOfEnterOnServer(item):
    global LastIpAddressInLog
    table = item.split('- -')
    ipAddress=table[0]
    if ipAddress[-1] is ' ': #usuwam spacje z adresu ip
        ipAddress=ipAddress[0:-1]
    if(ipAddress is LastIpAddressInLog):
        LastIpAddressInLog = ipAddress
    else:
        yield ipAddress, 1
        LastIpAddressInLog = ipAddress
        pass

def mapToCountingNumberOfDownloadingDataFromServer(item):
    table = item.split('- -')
    ipAddress=table[0]
    if ipAddress[-1] is ' ': #usuwam spacje z adresu ip
        ipAddress=ipAddress[0:-1]
    yield ipAddress, 1

def reduce(key, values):
    return (key, sum(values))

def test():
    print("STATYSTYKI WEJŚĆ NA SERWER:")
    mapper = MapReduce(mapToCountingTheNumberOfEnterOnServer, reduce)
    with open('access.log', 'rt') as f:
        table = f.readlines()
    results = mapper(table)
    for row in results:
        print "----------"
        print "Info: " + str(row)
    print("**********************************************************")
    print("STATYSTYKI POBIERANIA DANYCH Z SERWERA:")
    mapper = MapReduce(mapToCountingNumberOfDownloadingDataFromServer, reduce)
    with open('access.log', 'rt') as f:
        table = f.readlines()
    results = mapper(table)
    for row in results:
        print "----------"
        print "Info: " + str(row)
def pobierzInterwalPrzerwy(couch):
    db = couch['p2']
    print("Szukam w bazie danych wpisu na temat interwalu przerwy.")
    for id in db:
        for items in db[id].items():
            if 'interval' in items[0]:
                global Wait
                Wait = int(items[1])
                print ('Znaleziono w bazie couchDB interwal przerwy, wynoszacy: '+str(Wait))
                return

def wyczyscDaneZServera(couch):
    db = couch['p2']
    for id in db:
        db.delete(db[id])
    db.save({'interval':Wait}) #dodaje wpis o interwale, jako ze dopiero go usunalem :p

def wgrajNaSerwer(couch, table1, table2): #dodaje lub updateuje wpisy otrzymane w table1 i table2 (liczba wejsc i wielkosc sciagnietych danych)

    dict1 = dict()
    dict2 = dict()
    db = couch['p2']

    #wpierw spisuje dane z bazy danych do dwoch dictonary by latwiej było sprawdzac czy dane wpisy istnieją, by moc je updateowac
    for id in db:
        if not 'data' in db[id].items()[2][0]: #jesli to nie jest data, to jest wadliwy wpis(?) ignorij
            continue
        dataType = db[id].items()[2]#dostan sie do klucz/wartosc opisu jakie to dane
        item = db[id].items()[3]#dostan sie do klucz/wartosc danych dla danego ip

        if '_id' in item[0]: #by wyrzucic wadliwe wpisy ktore czasem sie przedostaja
            continue

        #robie slownik gdzie kluczem jest ip (co oznacza ze jest wpis w bazie na temat danego ip, a wartoscia item z bazy danych by moc go edytowac i wrzucic do bazy danych
        if int(dataType[1]) is 0: #jesli rowna sie 0 to sa to dane na temat liczby wejsc, a jesli 1 to dane na temat sciagnietych danych
            dict1[item[0]]=db[id]
        elif int(dataType[1]) is 1:
            dict2[item[0]]=db[id]

    print ("Liczba danych z serwera: "+str(len(dict1))+" + "+str(len(dict2)))
    global New, Updated, NotTouched
    New = 0
    Updated = 0
    NotTouched = 0
    #teraz majac dictonary, sprawdzam czy istnieje dane ip w slowniku, jesli tak to updateuje wpis jak nie to tworze nowy
    for item1 in table1:
        key=item1[0]
        value=item1[1]
        if key in dict1:
            dbitem = dict1[key]
            if dbitem.items()[3][1] is not  value: #jezeli wartosc jest rozna to update
                newtuple = tuple([dbitem.items()[3][0], value])
                dbitem.items()[3]=newtuple
                db.save(dbitem)
                Updated+=1
            else:
                NotTouched+=1
        else:
            if '_id' in key: #by pozbyc sie zlych danych ktore czasem sie przedostaja
                continue
            doc =  {'data': '0', key: value}
            db.save(doc)
            New+=1

    for item2 in table2:
        key=item2[0]
        value=item2[1]
        if key in dict2:
            dbitem = dict2[key]
            if dbitem.items()[3][1] is not value: #jezeli wartosc jest rozna to update
                newtuple = tuple([dbitem.items()[3][0], value])
                dbitem.items()[3]=newtuple
                db.save(dbitem)
                Updated+=1
            else:
                NotTouched+=1
        else:
            if '_id' in key: #usuwam wadliwe wpisy
                continue
            doc =  {'data': '1', key: value}
            db.save(doc)
            New+=1



if __name__ == '__main__':
    couch = couchdb.Server('http://194.29.175.241:5984')
    couch.resource.credentials = ('p2', 'p2')
    pobierzInterwalPrzerwy(couch)
    print("Poczatek programu odswiezajacago dane")
    print("Zaczynam od wyczyszczenia serwera.")

    while True:
        wyczyscDaneZServera(couch) #wyczysc na starcie dane - potrzebne, jako ze update kiepsko działa
        #odswiez dane - nowe polaczenie
        couch = couchdb.Server('http://194.29.175.241:5984')
        couch.resource.credentials = ('p2', 'p2')
        pobierzInterwalPrzerwy(couch)

        print("================")
        print("Kolejna iteracja: wgrywanie danych na serwer")
        mapper = MapReduce(mapToCountingTheNumberOfEnterOnServer, reduce)
        with open('/var/log/apache2/access.log', 'r') as f:
            table = f.readlines()
        results1 = mapper(table)
        mapper = MapReduce(mapToCountingNumberOfDownloadingDataFromServer, reduce)
        with open('/var/log/apache2/access.log', 'r') as f:
            table = f.readlines()
        results2 = mapper(table)
        print "Liczba danych z parsera: "+str(len(results1))+" + "+str(len(results2))

        wgrajNaSerwer(couch,results1,results2)
        global New, Updated, NotTouched
        print "Nowych wpisow: "+str(New)+", uzupelnionych: "+str(Updated)+", nie ruszonych: "+str(NotTouched)
        global Wait
        print("Koniec iteracji, czekam "+str(Wait)+" sekund.")
        time.sleep(Wait)

