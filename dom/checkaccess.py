import couchdb

def removekey(d, key):#by usuwac wpis z dictonary
    r = dict(d)
    del r[key]
    return r

def zmienInterwal(wait):
    couch = couchdb.Server('http://194.29.175.241:5984')
    couch.resource.credentials = ('p2', 'p2')
    db = couch['p2']
    print("Sprawdzam czy istnieje wpis na temat interwalu przerwy, jesli tak to go podmienie. Prosze o cierpliwosc, przeszukiwanie bazy troche trwa.")
    for id in db:
        if 'interval' in db[id].items()[2][0]: #jesli istnieje
            newtuple = tuple([db[id].items()[2][0], wait])
            db[id].items()[2]=newtuple
            db.save(db[id])
            print("Podmieniono wpis")
            return
    print("Stworzono nowy wpis")
    doc = {'interval': wait}
    db.save(doc)

def pobierzDaneIWyswietl(n1=10,n2=10): #Zrobic wydajniejsza wersje
    couch = couchdb.Server('http://194.29.175.241:5984')
    couch.resource.credentials = ('p2', 'p2')
    db = couch['p2']

    posortowane1=[]
    posortowane2=[]
    print("Prosba o cierpliwosc. Przechodzenie po bazie nie jest super wydajne. Dunno why. :( Tak z 1-2 min przelicza na laboratoryjnym.")
    #zczytuje dane do slownikow
    try:
        for id in db:
            if not 'data' in db[id].items()[2][0]: #jesli to nie jest data, to jest wadliwy wpis(?) ignorij
                continue

            dataType = db[id].items()[2]#dostan sie do klucz/wartosc opisu jakie to dane
            item = db[id].items()[3]#dostan sie do klucz/wartosc danych dla danego ip

     #jesli rowna sie 0 to sa to dane na temat liczby wejsc, a jesli 1 to dane na temat sciagnietych danych
            if '_id' in item[0]: #by wyrzucic wadliwe wpisy ktore czasem sie przedostaja
                continue
            if int(dataType[1]) is 0: #jesli rowna sie 0 to sa to dane na temat liczby wejsc, a jesli 1 to dane na temat sciagnietych danych
                posortowane1.append((item[1],item[0])) #jako ze sortowanie listy tuplow zaczyna sie po pierwszym elemencie dlatego przestawiam kolejnosc
            elif int(dataType[1]) is 1:
                posortowane2.append((item[1],item[0]))
    except couchdb.ResourceNotFound:
        print('Wystapil blad, najpewniej baza jest aktualizowana, sprobuj za chwile.')
        exit()

    posortowane1.sort()
    posortowane2.sort()

    print(str(n1)+' adresow IP uzytkownikow, ktorzy najczesciej sie laczyli (wraz z liczba polaczen).')
    for i in range(0,n1):
        print(str(i+1)+". "+posortowane1[-1-i][1]+" - "+str(posortowane1[-1-i][0]))
    print(str(n2)+' adresow IP uzytkownikow, ktorzy pobrali najwiecej danych (wraz z liczba pobranych danych).')
    for i in range(0,n2):
        print(str(i+1)+". "+posortowane2[-1-i][1]+" - "+str(posortowane2[-1-i][0]))

n1=10
n2=10
wait=180

while(True):
    print("===================")
    print("Witaj w aplikacji sprawdzajacej access.log.")
    print("By zakonczyc wcisnij CTRL+C")
    print("Wybierz akcje:")
    print("1. Wyswietl statystyki")
    print("2. Zmien liczbe adresow IP uzytkownikow, ktorzy najczesciej sie laczyli ("+str(n1)+")")
    print("3. Zmien liczbe adresow IP uzytkownikow, ktorzy pobrali najwiecej danych ("+str(n2)+")")
    print("4. Zmien czas odswiezania bazy couchdb ("+str(wait)+" sec)")
    choice = raw_input()
    print("===================")
    if choice == '1':
        pobierzDaneIWyswietl(n1,n2)
    elif choice == '2':
        n1=int(raw_input('Podaj liczbe adresow IP uzytkownikow, ktorzy najczesciej sie laczyli:'))
    elif choice == '3':
        n2=int(raw_input('Podaj liczbe adresow IP uzytkownikow, ktorzy pobrali najwiecej danych:'))
    elif choice == '4':
        wait = int(raw_input('Podaj czas odswiezania bazy couchdb:'))
        zmienInterwal(wait)

