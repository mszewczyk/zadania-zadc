# -*- coding: utf-8 -*-

import json
import string
from MapReduce import MapReduce


def map(item):
            table = item.split(',')
            id=table[1]
            yield id, table


def reduce(key, values):
    table = []
    tableForLineItem = []
    tableForOrder = []
    for val in values:
        tablename=val[0].split('[')[1].split("\"")[1]
        #print tablename
        if tablename=="order":
            tableForOrder.append(val)
        if tablename=="line_item":
            tableForLineItem.append(val)


    for val1 in tableForLineItem:
        for val2 in tableForOrder:
            table.append((val1, val2))


    return (key, table)


if __name__ == '__main__':
    #input_data = ...
    mapper = MapReduce(map, reduce)
    with open('records.json', 'rt') as f:
        table = f.readlines()
    results = mapper(table)
    #id = raw_input("podaj id:")
    for row in results:
        #if id==row[0]:
        print "----------"
        print "ID: " + row[0]
        for line in row[1]:
            print ""
            print line
        #print json.dumps(row)
