# -*- coding: utf-8 -*-

import multiprocessing
import string

from MapReduce import MapReduce


def map(filename):
    TR = string.maketrans(string.punctuation, ' ' * len(string.punctuation))

    print multiprocessing.current_process().name, 'czyta', filename

    with open(filename, 'rt') as f:
        for line in f:
            if line.lstrip().startswith('#'): # Pominięcie komentarzy
                continue
            line = line.translate(TR) # Pominięcie znaków przestankowych
            for word in line.split():
                word = word.lower()
                if word.isalpha():
                    yield word, 1


def reduce(word, occurances):
    return (word, sum(occurances))


if __name__ == '__main__':
    import operator
    import glob

    input_files = glob.glob('*.py')
    
    mapper = MapReduce(map, reduce)
    word_counts = mapper(input_files)
    word_counts.sort(key=operator.itemgetter(1))
    word_counts.reverse()
    
    print '\nNAJCZĘSTSZE 20 SŁÓW\n'
    top20 = word_counts[:20]
    longest = max(len(word) for word, count in top20)
    for word, count in top20:
        print '%-*s: %5s' % (longest+1, word, count)